# ODROID setup as companion computer

**Contents**

- Basic system configuration
- ROS
- OpenCV
- MAVlink, MAVproxy, etc

# Note: EDUP Wireless Adapter was not working for me (EP-DB1607). Used Wi-Pi instead. 

*Disable automatic updates*

Because this is annoying when running headless.
Locks up the package manager because of GUI notifications

		System > Administration > Software & Updates > Updates

*Update system*

		sudo apt-get update
		sudo apt-get upgrade
		sudo apt-get dist-upgrade
		sudo apt autoremove

*Correct system time*

		sudo dpkg-reconfigure tzdata

*Install odroid-utility*

This program makes easy various odroid configurations 

		sudo apt-get install curl

		sudo -s wget -O /usr/local/bin/odroid-utility.sh https://raw.githubusercontent.com/mdrjr/odroid-utility/master/odroid-utility.sh
		sudo chmod +x /usr/local/bin/odroid-utility.sh
		sudo odroid-utility

*Install ffmpeg*

Utilities/library for working with cameras, video files, etc

		sudo apt-get install ffmpeg

*Install ROS*

Robot Operating System. This section was based on Pixhawk documentation on recommended Odroid setup as a companion computer to a MAV. 

https://pixhawk.org/dev/ros/installation

		sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu xenial main" > /etc/apt/sources.list.d/ros-latest.list'
		wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -
		sudo apt-get update

		sudo apt-get install ros-kinetic-ros-base ros-kinetic-mavlink ros-kinetic-image-proc

		sudo apt-get install python-rosdep
		sudo rosdep init
		rosdep update

		echo "source /opt/ros/indigo/setup.bash" >> ~/.profile
		source ~/.profile

		mkdir -p ~/catkin_ws/src
		cd ~/catkin_ws/src
		catkin_init_workspace
		cd ..
		catkin_make #Even though the workspace is empty, it "compiles" which should tell you that your new setup is fine.
		echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc # auto sources workspace in new bash sessions
		source ~/.bashrc


*Install OpenCV*

Use this: http://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/

I followed it up to the step on downloading the contrib repo. 

*Setup headless (ssh & remote desktop)

You want to use remote desktop since the USB ports will typically have cameras attached.

Used this guide: http://odroid.com/dokuwiki/doku.php?id=en:headless_setup


## MAVlink, MAVproxy, etc

Source: http://ardupilot.org/dev/docs/odroid-via-mavlink.html

The MAV* suite of utilities provide communication between the autopilot and companion computer.
This can be either directly connected with an FTDI cable or wirelessly with telemetry.

**Disable ModemManager**

This service interferes with the telemetry, so it must be disabled.
Typically, tutorials request that you uninstall the package, but the package
is a dependency for the core Ubuntu Mate system. 

        service ModemManager stop
        systemctl disable ModemManager
        echo "manual" > /etc/init/modemmanager.override

Restart the system.

**Install software**

        sudo apt-get update    #Update the list of packages in the software center
        sudo apt-get install screen python-wxgtk3.0 python-matplotlib python-opencv python-pip python-numpy python-dev libxml2-dev libxslt-dev
        sudo pip install future
        sudo pip install pymavlink
        sudo pip install mavproxy
        sudo chown -R odroid /home/odroid

**Establish connection**

Either connect the telemetry radios to Pixhawk and ODROID,
or use the FTDI cable. 

Note that in the following code block, "MAV>" is your prompt when in MAVproxy. 
Do not type that part of the commands. When the prompt disappears, use 
'CTRL+C' to exit MAVproxy.

        mavproxy.py
        MAV> param show ARMING_CHECK
        MAV> param set ARMING_CHECK 0
        MAV> arm throttle

**Configure MAVProxy to always run**









