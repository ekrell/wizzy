## Mission Guide

Things to know before taking Wizzy out into the water.

*Critical:* Currently, Wizzy is still very much in testing. 
Some issues with waterproofness have been discovered, 
but this guide assumes a "normal" mission where the boat itself is 
assumed to be seaworthy. 

### Requirements

- At least two persons, who have been approved by Dr. King to handle the boat.
- Someone must be willing to get wet. Just because Evan Krell has a waterproof outfit does not mean he is the only person who can ever enter water. 
- Plenty of battery charge. The LIPOs take quite a while to charge, so its best to get ready the day before a mission.
- A suitable (King-approved) launch site. Wind/current must be toward shore in case of lost control with the boat.

### Check-list

- **boat**: with Pixhawk and whatever sensors, components required for mission
- **Spektrum RC** (radio controller): Must always be able to use remote control, as backup
- **batteries** (LIPOs): Carry separately from boat to reduce weight
- **towel**: To dry the top before opening boat
- **GCS laptop**: For QGroundControl
- **GCS radio dongle** (USB): Wireless communication between laptop (GCS) and Pixhawk
- **USB to micro-USB** cable: Wired communicaton between laptop (GCS) and Pixhawk - for debugging, recalibration, etc
- **sticky tack**: plug up wire-though indentions in the pelican cases holding electronics
- **tether** (ocean rescue reel): While boat still in testing phase, keep attached to tether in case control is lost

Print out this checklist table to make sure you have everything accounted for:

| Item | Taken Out | Returned |
| -------- | -------- | -------- |
| boat   | | |
| tether | | |
| Spektrum RC | | |
| batteries | | |
| towel | | |
| GCS laptop | | |
| GCS radio dongle | | |
| USB to micro-USB | | |
| sticky tack | | |

### Launch Sites

Remember to shuffle your feet in the water to avoid stingrays.

**Ward Island Launch 1**

![](images/launchSite1.png)

Be mindful of the drain. If you step in front of it, it is suddenly very deep.
An overwhelming smell of that which has been drained overcomes you.

Quite grassy when we were out (July 2017): underwater sensors quickly covered in algae. 

**University Beach Launch**

![](images/launchSite2.png)

The part right of the vertical extension (breakwater) is easy to access and deep enough to place the boat directly in water from shore. Highly recommended.

The left side is appealing because it is protected from multiple surrounding breakwaters, but is very lightly. Less useful, especially at low tide. 

In blue is the recommended path from the EN building to the launch site. 
