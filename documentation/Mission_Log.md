## Mission Log

**Tuesday, 01/8/2017**

Goals

- Test towing a platform

![](images/towing.jpg)

![](images/remoteControl.jpg)

![](images/wizzyReturnsToShore.jpg)

![](images/universityBeach.jpg)

![](images/platform.jpg)

Joe Lopez built a platform that will be used for microlayer sampling.
Its hard to see in the images, but it is boxed in yellow above. 
We wanted to see if the boat will tow this platform.
Evan Krell, Joe Lopez, and Dr. James Silliman went out to **University Beach Launch**.
(See documantation/Mission_Guide.md for launch site information).
The platform had considerable drag, but the boat was able to make multiple complete circles. 
Twice, the boat began looking as though it would flip, but through careful manual control this was avoided.
This means that autonous towing needs to programmed to account for drag.
If the boat turns too sharply without slowing down, it may turn over. 
We determined that the platform needs to be modified to reduce drag, and also
considered the idea of pontoons strapped to the boat itself for stability. 
Also, Dr. Chris Bird later advised attaching the platform to the boat in two places, making a Y-shape. 


**Monday, 10/7/2017**

Goals

- Test waterproofing attempts

- Test remote control

- Test GPS

- Test waypoint navigation with Pixhawk and QGroundControl

![](images/mission_2017-7-10.png)

In the first mission, we experienced the tragedy of equipment damage as the 
Pixhawk power module perished under the sting of saltwater. 
This led to the use of Pelican cases for holding all added electronics.
The factory model boat has marine-quality components, so it is only the additional
modules such as the Pixhawk flight controller, GPS, radio receiver, etc that are of concern.
We also used PVC insulator to seal the hatch.
In this mission, __very little water entered the boat and no damage was done.__
There are wells near the back that catch water and the portside well had a pool of water.
Since no where else was there any evidence of water, it seems that it did not come from the
hatch, but rather poor sealing of equipment that goes through the boat around that area.

__Remote control worked very well__.

We __achieved GPS lock__ on the boat, which is needed for the waypoint navigation. 

However, we were not able to maintain a stable connection between the laptop and Pixhawk radio receiver/transmitters,
so __waypoint navigation was not possible__. Need to check how this was handled with the EMILY USV, 
which also uses Pixhawk and waterproof containers.

Borrowed a wagon for transport from the lab to launch site.
It fit in the elevator with the boat. Took ~17 minutes to make the journey, but could be
much faster if straps were used to secure the boat. 
Was able to carry the boat, safety rope, and dry bag with electronics.