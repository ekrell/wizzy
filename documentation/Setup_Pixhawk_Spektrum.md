## Setup: Pixhawk autopilot with Spektrum remote control

Wizzy uses the Pixhawk mini for autonomous navigate 
and the Spektrum radio controller for remote control. 


### Hardware Setup: 

![](images/Wizzy_Modules.png)

*Preferably both photo and diagram*

*Also, how to place in the waterproof cases*

### Software Setup:

1. Download and install [QGroundControl](qgroundcontrol.com)

2. Configure the Pixhawk 

    Before you can deploy Wizzy, the Pixhawk must have proper firmware and calibration.
    This is described in the [quick start guide](docs.qgroundcontrol.com/en/getting_started/quick_start.html).
    However there are a couple issues that we ran into, described below.

    1. Airspeed Sensor Not Found
    
        Since Wizzy is a boat, it does not need an airpeed sensor. 
     Pixhawk, even when set to rover, required an airspeed sensor before it will arm.
        This can be overriden by going to Settings -> Circuit Breakers -> AirSpeed Check.
        Follow the directions to disable the check. 
    
    2. ESC and rudder control directions flipped
    
        When calibrating the Spektrum controller within QGroundControl,
        the controls you manipulate for throttle and rudder appear correct as they match up with 
        the visual indicator that displays your controller movements. 
        However, once attempting to use remote control.. one throttle is horizontal and rudder vertical. 
    
        We solved this, temporarily, by doing the opposite of what we were supposed to do in QGroundControl.
        This worked very well for remote control, but we fear that the autopilot may be incorrect since Pixhawk doesn't 
        know to be opposite. Further testing required to find the best approach for a work-around. 
    
3. Calibrate ESC

    1. Power on Pixhawk. Arm with safety switch. Hold throttle stick to full throttle. 
    2. Power on boat. Wait for N quick beeps. Then hold throttle stick to zero. Wait for single long beep. 
    3. Should have remote control over throttle and rudder. 