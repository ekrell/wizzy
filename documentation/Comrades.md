**This is a guide for other boats that support the Wizzy research effort.**

## Smart-EMILY

![](images/comrades/emily.jpg)

A life-saving bouy robot from emilyrobot.com that has been modified by [CRASAR](http://crasar.org) to support autonomous waypoint navigation.

Currently on loan from [CRASAR](http://crasar.org). 

Operation is similar to Wizzy, as both use Pixhawk for waypoint navigation.
However, EMILY is configured to work with Mission Planner rather than QGroundControl as its ground control software. 
In this document, will refer to Smart-Emily as simply "Emily".

### Smart-Emily mission guide

**Requirements**

- At least two persons, who have been approved by Dr. King to handle the boat.
- Someone must be willing to get wet.
- Plenty of battery charge. The LIPOs take quite a while to charge, so its best to get ready the day before a mission.
- A suitable (King-approved) launch site. Approved launch sites are in the [Wizzy mission guide](documentation/Mission_Guide.md)

**Gear & Check-list**

The Emily comes with two cases for carrying the boat and gear.
The blue backpack is for most of the small items. 
The red case is for the Emily itself and the remote control.
These will be refered to as "blue case" and "red case", respectively.

![](images/comrades/emilyGear.jpg)

![](images/comrades/emilyBackpack.jpg)

- **Remote Control**: Must always be able to use remote control, as backup
- **batteries**: LIPOs
- **yellow towel**: To dry boat after washing with fresh water
- **red towel**: To pad the boat within the red case
- **PVC support**: To keep boat secure within the red case
- **GCS laptop**: With Mission Planner installed
- **GCS radio dongle** (USB): Wireless communication between laptop (GCS) and Pixhawk
- **USB to micro-USB** cable: Wired communicaton between laptop (GCS) and Pixhawk - for debugging, recalibration, etc
- **tether** (ocean rescue reel): While boat still in testing phase, keep attached to tether in case control is lost

Print out this checklist table to make sure you have everything accounted for:

| Item | Case | Taken Out | Returned |
| -------- | -------- | -------- | -------- |
| [A] batteries | blue | | |
| [B] battery charger | blue | | |
| [C] Emily manual | blue | | | 
| [D] battery charging manual | blue | | |
| [E] thin rope | blue | | | 
| [F] red blow thing | blue | | | 
| [G] tool box | blue | | | 
| [H] I2C Splitter Expand Modules | blue | | | 
| [I] Playstation 2-style controller (USB) | blue | | | 
| [J] Xbox-style controller (USB) | blue | | | 
| [K] USB to micro-USB | blue | | |
| [L] GCS radio dongle | blue | | |
| Emily | red | | | 
| Remote Control | red | | |
| PVC support | red | | |
| red towel | NA | | |
| yellow towel | NA | | | 
| GCS laptop | NA | | |
| tether | NA | | |

**Storing Emily**

Use red towel to support the aft.

![](images/comrades/emilyStorage1.jpg)

Use the PCV support for the bow, and store the remote controller under.

![](images/comrades/emilyStorage2.jpg)







