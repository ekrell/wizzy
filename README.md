## Wizzy
Wizzy (Water Izzy) is RAVE's work-in-progress unmanned surface vehicle. 
The goal is to create a platform for sensors that can be used in a variety of autonomous missions. 

## Overview
The base platform is the [Pro Boat Zelo 48 catamaran](http://www.proboatmodels.com/Products/Default.aspx?ProdId=PRB08017).
It has been outfitted by Dr. Eduardo Quesada with a [Pixhawk mini](https://store.3dr.com/products/3dr-pixhawk) autopilot,
an [Odroid-XU4](hardkernel.com/main/products/prdt_info.php?g_code=G143452239825) as companion computer for onboard processing, 
and armature/servos for sensors above and below the surface. 
Current sensors are two [oCams](hardkernel.com/main/products/prdt_info.php?g_code=G145231889365) and a GoPro 

## Status

See: documentation/Mission_Log.md


## The Team

**Graduate Students**

- Evan Krell - evan.krell@tamucc.edu

- Shubharaj Arsekar - sarsekar@islander.tamucc.edu

- Arun Prassanth - aramaswamybalasubram@islander.tamucc.edu

**Advising Professors**

- Dr. Scott King

- Dr. Luis Garcia Carrillo

- Dr. Chris Bird

- Dr. Eduardo Steed Espinoza Quesada



